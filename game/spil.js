// Variable declaration
console.log("Hello there");
wordList = [
    "TRÆLS",
    "SUR",
    "IRRITERENDE",
    "DUM",
    "GRIM",
    "SØD",
    "DYGTIG",
    "SJOV", 
    "PÆN",
    "SEJ", 
    "COOL"
];
let nrOfWords = 5;
let wordDistance = 70;
let updateTime = 50;
activeWords = [];
document.body.style.backgroundColor = "#329A79"; 
document.body.getElementsByClassName("header");
document.body.getElementsByClassName("image");

// Game area variable
var gameArea = {
    cnvs : document.createElement("canvas"),

    // Starts game
    start : function() {
        this.cnvs.width = 1450;
        this.cnvs.height = 969;
        this.context = this.cnvs.getContext("2d");
        this.context.font = '40px Lilita One';
        document.body.insertBefore(this.cnvs,document.body.childNodes[0]);
        initializeWordList(activeWords);
        this.interval = setInterval(updateGame,updateTime);
    },

    // Clears area
    clear : function() {
        this.context.clearRect(0, 0, this.cnvs.width, this.cnvs.height);
    },
}

// Word object declared as function
function wordObject(textString, x, y){
    this.textString = textString;
    this.x = x;
    this.y = y;
    this.moveDown = function(){
        this.y += 3;
    }
    this.drawWord = function(){
        ctx = gameArea.context;
        ctx.fillText(this.textString,this.x,this.y);
    }
    this.getY = function(){
        return this.y;
    }
}

function initializeWordList(activeWords){
    for (let i = 0; i < nrOfWords;i++){
        z = Math.floor(Math.random()*wordList.length);
        x = Math.floor(Math.random()*gameArea.cnvs.width);
        y = (i) * -wordDistance;

        activeWords[i] = new wordObject(wordList[z],x,y);
    }
}

// Keeps list of active words up to date
function updateActiveWordsList(activeWords){
    // Remove any words that have reached the bottom
    console.log(activeWords[0].getY());
    if (activeWords[0].getY() >= (gameArea.cnvs.height + 30)){
        console.log("Removing item");
        activeWords.shift();
        z = Math.floor(Math.random()*wordList.length);
        x = Math.floor(Math.random()*gameArea.cnvs.width);
        y = 0;

        activeWords[nrOfWords-1] = new wordObject(wordList[z],x,y);
    }

}

function moveWords(activeWords){
    for(let i = 0; i < activeWords.length;i++){
        activeWords[i].moveDown();
    }
}

function drawWords(activeWords){
    for (let i = 0; i < activeWords.length;i++){
        activeWords[i].drawWord();
    }
}

// Runs for every frame
function updateGame() {
    updateActiveWordsList(activeWords);
    gameArea.clear();
    moveWords(activeWords);
    drawWords(activeWords);
}

// Starts game:
gameArea.start();